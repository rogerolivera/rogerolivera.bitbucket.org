var Twitter = (function(){

    var shareButton = function(text, url, hashtags){
        var twitterContent = document.getElementById('twitterContent');

        var a = document.createElement('a');
        a.setAttribute('class', 'twitter-share-button');
        a.setAttribute('href', 'https://twitter.com/share');
        a.setAttribute('data-size', 'large');
        a.setAttribute('data-text', text);
        a.setAttribute('data-url', url);
        a.setAttribute('data-hashtags', hashtags);
        a.setAttribute('data-via', 'twitterdev');
        a.setAttribute('data-related', 'twitterapi,twitter');
        a.textContent = 'Twitter';

        twitterContent.appendChild(a);

    }

    return{
        'shareButton': shareButton
    }

    /*<a class="twitter-share-button"
  href="https://twitter.com/share"
  data-size="large"
  data-text="queens of kpop"
  data-url="https://www.youtube.com/watch?v=LUrUPzLm5SI"
  data-hashtags="2NE1,dara,cl,bom,minzy"
  data-via="twitterdev"
  data-related="twitterapi,twitter">
Tweet
</a>
  </body>*/
})();